# opti-sway-i3wm-mate-labwc

####################################

- opti-sway-i3wm-mate-labwc-version: nov 2023

- build-latest: 0.0.6

# For FreeBSD

- opti-sway-i3wm-mate-labwc-freebsd-version: nov 2023

- build-latest: 0.0.3

- On FreeBSD, install bash to have no issues.

- To resolve the issue of the opti-sway-i3wm-mate-labwc error, even with bash, use the following command: `sudo ln -s /usr/local/bin/bash /bin/`

####################################

- Support for the distro: Void-Linux/Ubuntu/Debian/Arch/Artix/Manjaro/FreeBSD

- If you are using a distribution based on Ubuntu, Debian, or Arch, the script will work without any issues.

- Use at your own risk

- Starting from version 0.0.1, executing opti-sway-i3wm-mate-labwc as a superuser or with sudo privileges has been blocked. It can only be executed without superuser or sudo privileges

- From version 0.0.5-Linux/0.0.2-FreeBSD onwards, it now includes support for MATE

- From version 0.0.6-Linux/0.0.3-FreeBSD onwards, it now includes support for LABWC

- opti-sway-i3wm-mate-labwc is an open-source project, and we are happy to share it with the community. You have complete freedom to do whatever you want with opti-sway-i3wm-mate-labwc, in accordance with the terms of the MIT license. You can modify, distribute, use it in your own projects, or even create a fork of opti-sway-i3wm-mate-labwc to add additional features.

## Installation

- To install opti-sway-i3wm-mate-labwc, follow the steps below:

# 1. Clone this repository by running the following command

- git clone https://gitlab.com/manoel-linux1/opti-sway-i3wm-mate-labwc.git

# 2. To install the opti-sway-i3wm-mate-labwc script, follow these steps

- chmod a+x `installupdate.sh`

- sudo `./installupdate.sh`

# 3. Execute the opti-sway-i3wm-mate-labwc script

For Linux

- `opti-sway-i3wm-mate-labwc`

For FreeBSD

- `opti-sway-i3wm-mate-labwc-freebsd`

# For uninstall

- chmod a+x `uninstall.sh`

- sudo `./uninstall.sh`

# Other Projects

- If you found this project interesting, be sure to check out my other open-source projects on GitLab. I've developed a variety of tools and scripts to enhance the Linux/BSD experience and improve system administration. You can find these projects and more on my GitLab: https://gitlab.com/manoel-linux1

# Project Status

- The opti-sway-i3wm-mate-labwc project is currently in development. The latest stable version is 0.0.6-Linux/0.0.3-FreeBSD We aim to provide regular updates and add more features in the future.

# License

- opti-sway-i3wm-mate-labwc is licensed under the MIT License. See the LICENSE file for more information.

# Acknowledgements

- We would like to thank the open-source community for their support and the libraries used in the development of opti-sway-i3wm-mate-labwc.
