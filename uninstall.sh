#!/bin/bash

clear
echo "#################################################################"
echo "(opti-sway-i3wm-mate-labwc-uninstaller) >> (nov 2023)"
echo "#################################################################"
echo " ███████  ██████ ██████  ██ ██████  ████████ "
echo " ██      ██      ██   ██ ██ ██   ██    ██    "
echo " ███████ ██      ██████  ██ ██████     ██    "
echo "      ██ ██      ██   ██ ██ ██         ██    "
echo " ███████  ██████ ██   ██ ██ ██         ██    "   
echo "#################################################################"
echo "(opti-sway-i3wm-mate-labwc-gitlab) >> (https://gitlab.com/manoel-linux1/opti-sway-i3wm-mate-labwc)"
echo "#################################################################"

if [[ $EUID -ne 0 ]]; then
echo " ███████ ██████  ██████   ██████  ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██ ██ "
echo " █████   ██████  ██████  ██    ██ ██████  ██ "
echo " ██      ██   ██ ██   ██ ██    ██ ██   ██    "
echo " ███████ ██   ██ ██   ██  ██████  ██   ██ ██ "                                                                                        
echo "#################################################################"
echo "(Superuser privileges or sudo required to execute the script)" 
echo "#################################################################"
exit 1
fi

clear

sudo rm /usr/bin/opti-sway-i3wm

sudo rm /usr/bin/opti-sway-i3wm-mate

sudo rm /usr/bin/opti-sway-i3wm-mate-labwc

sudo rm /usr/bin/opti-sway-i3wm-freebsd

sudo rm /usr/bin/opti-sway-i3wm-mate-freebsd

sudo rm /usr/bin/opti-sway-i3wm-mate-labwc-freebsd

clear

echo "#################################################################"
echo " ██████   ██████  ███    ██ ███████ ██ "
echo " ██   ██ ██    ██ ████   ██ ██      ██ "
echo " ██   ██ ██    ██ ██ ██  ██ █████   ██ "
echo " ██   ██ ██    ██ ██  ██ ██ ██         "
echo " ██████   ██████  ██   ████ ███████ ██ " 
echo "#################################################################"
echo "(Uninstallation completed)"
echo "#################################################################"
